var express = require("express"),
    app     = express(),
    bodyParser = require("body-parser");

//Body-Parser config

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));


//Config file route
var config = require('./config/config');

//Routes Path
var routes = require('./routes/routes');

//Using Routes Here
app.use(routes);

//Starting Server Here

app.listen(config.port, config.host, () => {
    console.log(`server is running at http://${config.host}:${config.port}`);
});