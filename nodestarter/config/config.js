//Config object

var config = {};

//Setting Server Host and Process

config.host = '0.0.0.0';
config.port = process.env.WEB_PORT || 9091;

//MongoDb DB connections config

config.mongoDB = {};
config.mongoDB.user = 'someusername';
config.mongoDB.password = 'somepassword';

//Exporting this module

module.exports = config;