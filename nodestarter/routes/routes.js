var express = require("express"),
    router = express.Router();

var landingController = require('../controllers/landing');
var landingPostController = require('../controllers/landingPost');


//Get Route for landing

router.get('/', landingController.landing);

router.post('/post', landingPostController.landingPost);


module.exports = router;